# kyverno

[Kyverno](https://kyverno.io/) is a policy engine designed for Kubernetes.

__Platform Administrators__ use Kyverno for enforcing policies on Kubernetes resources.

Kyverno is installed using the Helm chart.

Links:
- Chart sources and flags: https://github.com/kyverno/kyverno/tree/main/charts/kyverno
- Releases: https://github.com/kyverno/kyverno/releases
- Docs: https://kyverno.io/docs/

# Contents

## Current Version
Current version of kyverno is installed with helm chart version 2.1.3. It installs version 1.5.1 of kyverno.

For releasenotes see https://kyverno.io/docs/releases/#kyverno-v151

# Local installation

## Install Kyverno

Get platform runner:
```bash
docker pull registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest
```

Connect to Kubernetes cluster, e.g.:
```bash
export KUBECONFIG=$HOME/.kube/config
```

In working directory, add or extend `cluster_config.yaml` ->

```yaml
platform:
  prefix: local
  domain: example.com # Use your own domain

config:
  kyverno:
    # no specific kyverno configuration needed
```

From working directory, install Kyverno:
```bash
# Prepare Ansible command, assuming above cluster_config.yaml will be mounted below
ANSIBLE_CMD="ansible-playbook run-role.yml \
  -e @/playbook/configfiles/cluster_config.yaml \
  -e galaxy_url=https://gitlab.com/logius/cloud-native-overheid/components/kyverno \
  -vv"

# Run Ansible role, using ANSIBLE_CMD with mounted cluster config
docker run --rm \
  -v $KUBECONFIG:/home/builder/.kube/config \
  -v $PWD/cluster_config.yaml:/playbook/configfiles/cluster_config.yaml \
  -e ANSIBLE_FORCE_COLOR=true \
  --network=host \
  registry.gitlab.com/logius/cloud-native-overheid/components/platform-runner:latest \
  $ANSIBLE_CMD
```

## Troubleshooting
### rollback
Rollback command:
`helm rollback  kyverno --namespace sp-kyverno`
